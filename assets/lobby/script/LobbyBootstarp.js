cc.Class({
    extends: cc.Component,

    onLoad: function () {
        this._renderPanel();
    },

    _renderPanel: function () {
        var lobby_lbi_name = this.node.getChildByName("lobby-lbi-name");
        lobby_lbi_name.getComponent(cc.Label).string = ACCOUNT.name;
    },
});
