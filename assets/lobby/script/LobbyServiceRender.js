cc.Class({
    extends: cc.Component,

    // use this for initialization
    onLoad: function () {
        var self = this;
        var selfNode = this.node;

        ACCOUNT.profiles.push({
            'name': '斗地主',
            'serviceName': 'ddz',
            'open': true
        });

        ACCOUNT.profiles.push({
            'name': '添加游戏',
            'serviceName': 'add',
            'open': true
        });

        cc.loader.loadRes("lobby/prefab/lobby-page", function (err, pageRes) {
            if (err) {
                cc.log(err);
                return;
            }

            cc.loader.loadRes("lobby/prefab/lobby-service", function (err, serviceRes) {

                if (err) {
                    cc.log(err);
                    return;
                }

                cc.loader.loadRes("lobby/textures/icon", cc.SpriteAtlas, function (err, iconRes) {
                    if (err) {
                        cc.log(err);
                        return;
                    }

                    self._renderPage(self, selfNode, pageRes, serviceRes, iconRes);
                });
            });
        });
    },
    
    _renderPage: function (self, selfNode, pageRes, serviceRes, iconRes) {

        

        var width = selfNode.width;
        var height = selfNode.height+100;
        var xTotalService = 5;
        var yTotalService = 3;
        var widthStep = width / (xTotalService+1);
        var heightStep = height / (yTotalService+1);
        var pageNum = xTotalService*yTotalService;

        //创建 pageview 的页数
        var totalPage = Math.ceil(ACCOUNT.profiles.length / pageNum);
        for (var i = 0; i < totalPage; i++) {
            var labby_page_node = cc.instantiate(pageRes);
            self.getComponent(cc.PageView).addPage(labby_page_node);

            var start = i * pageNum;
            var end = start + pageNum;

            if (end > ACCOUNT.profiles.length) {
                end = ACCOUNT.profiles.length;
            }

            //创建 pageview 每页的展示元素
            var xCount = 0, yCount = 0;
            for (var j = start; j < end; j++) {
                var profileInfo = ACCOUNT.profiles[j];

                var labby_service_node = cc.instantiate(serviceRes);
                var labby_service_node_spr = labby_service_node.getComponent(cc.Sprite);
                labby_service_node_spr.spriteFrame = iconRes.getSpriteFrame("icon-" + profileInfo.serviceName);
                labby_service_node.x = widthStep+ widthStep * (xCount % xTotalService);
                labby_service_node.y = -heightStep*0.5 -heightStep * yCount;
                labby_page_node.addChild(labby_service_node);
                
                var lab = labby_service_node.getChildByName("lobby-service-name");
                lab.getComponent(cc.Label).string = profileInfo.name;

                labby_service_node.on(cc.Node.EventType.TOUCH_END, function () {

                    var lab = this.getChildByName("lobby-service-name");
                    if (lab.string == "add") {
                        //TODO 弹开选择列表
                        cc.log("open add page");
                    } else {
                        //TODO 加载游戏页面 
                        cc.log("open service" + lab.string);
                    }
                }.bind(labby_service_node));

                //位置索引控制
                xCount++;
                if(xCount%xTotalService==0){
                    yCount++;
                }
            }
                        
        }
    }

});
