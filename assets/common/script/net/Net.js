var Net = {
    socket: null,
}

Net.doConnect = function (url, onConn, onReceive, onClose, onError) {

    if (cc.sys.isObjectValid(this.socket) && this.socket.readyState < 2) {
        throw "the socket still connect, please disConnect first";
    }

    this.socket = new WebSocket(url);
    this.socket.binaryType = 'arraybuffer';

    this.socket.onopen = onConn;
    this.socket.onmessage = onReceive;
    this.socket.onclose = onClose;
    this.socket.onerror = onError;
}
Net.disConnect = function () {
     if (cc.sys.isObjectValid(this.socket) && this.socket.readyState < 2) {
        cc.log("to disconnect");
        this.socket.close();
    }
}
Net.send = function (data) {
    if (data instanceof ArrayBuffer) {
        this.socket.send(data);
    } else {
        throw "Net send param [data] is not ArrayBuffer obj";
    }
}
Net.isConnectd = function () {
    return cc.sys.isObjectValid(this.socket) &&this.socket.readyState === 1;
}


module.exports = Net;