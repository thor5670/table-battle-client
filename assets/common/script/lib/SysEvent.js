window.SysEvent = {
    linistener:{},
    on:function(eventName,callback,context){
        
        if(typeof eventName == 'undefined' || eventName==null || "" == eventName){
            throw "eventName can not be null";
        }

        if(typeof callback == 'undefined' || eventName == null){
            throw "callback can not be null";
        }
        
        if(typeof this.linistener[eventName] == 'undefined'){
            this.linistener[eventName] = new Array();           
        }

        this.off(eventName,callback);

        this.linistener[eventName].push({'callback':callback,'context':context});
    },
    off:function(eventName,callback){
        if(typeof eventName == 'undefined' || typeof callback == 'undefined'){
            return;
        }
        
        var ltrlist = this.linistener[eventName];
        if(typeof ltrlist == 'undefined'){
            return;
        }

        for(var i=0;i<ltrlist.length;i++){
            var callInfo = ltrlist[i];
            if(callInfo.callback == callback){
                ltrlist.splice(i,1);
            }
        }
    },
    emit:function(eventName,param){
        var ltrlist = this.linistener[eventName];

        if(typeof ltrlist == 'undefined'){
            return;
        }

        for(var callInfo in ltrlist){
            callInfo.callback.apply(callInfo.context,param);
            callInfo.callback();
        }
    }

}