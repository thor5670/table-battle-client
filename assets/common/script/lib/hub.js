/**
 * 追加 string.format 函数
 * 
 * 通过 "xxx%sxxx%s".format("s1","s2") 样式格式化字符串
 */
String.prototype.format = function () {
    var args = arguments;
    var i = 0;
    return this.replace(/%(%|s|d)/g, function (m) {
        // m is the matched format, e.g. %s, %d
        var val = args[i];
        // A switch statement so that the formatter can be extended. Default is %s
        switch (m) {
            case '%d':
                val = parseFloat(val);
                if (isNaN(val)) {
                    val = 0;
                }
                break;
        }
        i++;
        return val;
    });
};

/**
 * 追加函数 dump
 * 
 * 将一个js对象以一定结构输出到控制台
 */
window.dumpStr = function (data) {

    if( typeof data == 'undefined'){
        return "data is undefined";
    }else if(data == null){
        return "data is null";
    }


    var appendInnerObject = function(objStr){
        var attArray = objStr.split(SYMBOL_NEW_LINE);
        for(var i=0;i<attArray.length;i++){
            if(attArray[i]!= ""){
                dumpInfo += "- - "+attArray[i]+SYMBOL_NEW_LINE;
            }
        }
    }

    var dumpInfo = "- " + data.constructor.name + " : object" + SYMBOL_NEW_LINE;
    if (typeof data == 'object') {
        for (var item in data) {

            if (typeof data[item] == "function") {
                continue;
            }

            if (typeof data[item] == 'object') {
                if(data[item] instanceof Array){
                    dumpInfo += "- - "+item+" : array ("+data[item].length+")"+SYMBOL_NEW_LINE;
                    for(var arrayItem in data[item]){
                        var innerObj =  dumpStr(data[item][arrayItem]);
                        appendInnerObject(innerObj);
                    }
                }else{
                    dumpInfo += "- - - " + dumpStr(data[item]);
                }
            } else {
                dumpInfo += "- - " + item + " : " + data[item] + SYMBOL_NEW_LINE;
            }
        }
    }
    return dumpInfo;
}

window.dump = function(data){
    cc.log(dumpStr(data));
}

/**
 * 追加 arraybuffer.transfer 函数
 * 
 * 将一个arraybuff复制到一个拥有新长度的arraybuf
 */
if (!ArrayBuffer.transfer) {
    ArrayBuffer.transfer = function (source, length) {
        source = Object(source);
        var dest = new ArrayBuffer(length);
        if (!(source instanceof ArrayBuffer) || !(dest instanceof ArrayBuffer)) {
            throw new TypeError('Source and destination must be ArrayBuffer instances');
        }
        if (dest.byteLength >= source.byteLength) {
            var nextOffset = 0;
            var leftBytes = source.byteLength;
            var wordSizes = [8, 4, 2, 1];
            wordSizes.forEach(function (_wordSize_) {
                if (leftBytes >= _wordSize_) {
                    var done = transferWith(_wordSize_, source, dest, nextOffset, leftBytes);
                    nextOffset = done.nextOffset;
                    leftBytes = done.leftBytes;
                }
            });
        }
        return dest;
        function transferWith(wordSize, source, dest, nextOffset, leftBytes) {
            var ViewClass = Uint8Array;
            switch (wordSize) {
                case 8:
                    ViewClass = Float64Array;
                    break;
                case 4:
                    ViewClass = Float32Array;
                    break;
                case 2:
                    ViewClass = Uint16Array;
                    break;
                case 1:
                    ViewClass = Uint8Array;
                    break;
                default:
                    ViewClass = Uint8Array;
                    break;
            }
            var view_source = new ViewClass(source, nextOffset, Math.trunc(leftBytes / wordSize));
            var view_dest = new ViewClass(dest, nextOffset, Math.trunc(leftBytes / wordSize));
            for (var i = 0; i < view_dest.length; i++) {
                view_dest[i] = view_source[i];
            }
            return {
                nextOffset: view_source.byteOffset + view_source.byteLength,
                leftBytes: source.byteLength - (view_source.byteOffset + view_source.byteLength)
            }
        }
    };
}