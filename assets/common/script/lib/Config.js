/**
 * route url
 */
window.ROUTE_URL = "ws://127.0.0.1:10000/websocket";

/**
 *  proxy url 模型
 */
window.PROXY_URL = "ws://%s:%s/websocket";

/**
 * debug
 */
window.DEBUG = true;

/**
 * 换行符
 */
window.SYMBOL_NEW_LINE = "\n";

/**
 * module mapping 动态的,由协议处理模块追加
 */
window.MODULE_MAPPING = {};

/**
 * 账户信息
 */
window.ACCOUNT={};

//####################################
//######      系统事件定义    ##########
//####################################

/**
 * 网络断开
 */
window.EVENT_NET_CONNERROR = "CONN_ERROR";

/**
 * 网络慢
 */
window.EVENT_NET_SLOW = "CONN_SLOW";


