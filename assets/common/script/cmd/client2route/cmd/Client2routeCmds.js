
//##############################
//####RouteProxy
//##############################

var RouteProxyRequest = function(){
}

RouteProxyRequest.prototype.readFrom = function (byteBuf){
}

RouteProxyRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x010001);
}

RouteProxyRequest.prototype.getCommandId = function () {
       return 0x010001;
}

module.exports.RouteProxyRequest = RouteProxyRequest;

var RouteProxyResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

   /**
   *   分配到的代理地址
   **/
   this.ip = "";

   /**
   *   代理地址端口
   **/
   this.port = 0;

}

RouteProxyResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
       this.ip = byteBuf.readUTF8();
       this.port = byteBuf.readInt();
}

RouteProxyResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x010001);
       byteBuf.writeInt(this.errorNo);
       byteBuf.writeUTF8(this.ip);
       byteBuf.writeInt(this.port);
}

RouteProxyResponse.prototype.getCommandId = function () {
       return 0x010001;
}

module.exports.RouteProxyResponse = RouteProxyResponse;
