var cmds = require("Client2routeCmds");

var RouteHandler = function(scence){


    this.scence = scence;
    this.servicename="client2route";

    this.exec = function(commandId,bytebuf){
      switch(commandId){
           case 0x010001 :{
               var rsp = new cmds.RouteProxyResponse();
               rsp.readFrom(bytebuf);
               this.handlerRouteProxyResponse(rsp);
               break;
           }
        }
      }

    //模块处理映射注册
    MODULE_MAPPING[0X01] = this;

}

module.exports = RouteHandler;
