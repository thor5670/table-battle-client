var cmds = require("Client2proxyCmds");

var Client2ProxyHandler = function(scence){


    this.scence = scence;
    this.servicename="client2proxy";

    this.exec = function(commandId,bytebuf){
      switch(commandId){
           case 0x020001 :{
               var rsp = new cmds.ProxyMsgResponse();
               rsp.readFrom(bytebuf);
               this.handlerProxyMsgResponse(rsp);
               break;
           }
           case 0x020002 :{
               var rsp = new cmds.HeartBeatResponse();
               rsp.readFrom(bytebuf);
               this.handlerHeartBeatResponse(rsp);
               break;
           }
        }
      }

    //模块处理映射注册
    MODULE_MAPPING[0X02] = this;

}

module.exports = Client2ProxyHandler;
