
//##############################
//####ProxyMsg
//##############################

var ProxyMsgRequest = function(){
   /**
   *   服务名称
   **/
   this.service = "";

   /**
   *   服务参数
   **/
   this.allotParam = "";

   /**
   *   协议数据
   **/
   this.msg = new Array();

}

ProxyMsgRequest.prototype.readFrom = function (byteBuf){
       this.service = byteBuf.readUTF8();
       this.allotParam = byteBuf.readUTF8();
       this.msg = byteBuf.readByteArray();
}

ProxyMsgRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x020001);
       byteBuf.writeUTF8(this.service);
       byteBuf.writeUTF8(this.allotParam);
       byteBuf.writeByteArray(this.msg);
}

ProxyMsgRequest.prototype.getCommandId = function () {
       return 0x020001;
}

module.exports.ProxyMsgRequest = ProxyMsgRequest;

var ProxyMsgResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

   /**
   *   协议处理结果
   **/
   this.result = new Array();

}

ProxyMsgResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
       this.result = byteBuf.readByteArray();
}

ProxyMsgResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x020001);
       byteBuf.writeInt(this.errorNo);
       byteBuf.writeByteArray(this.result);
}

ProxyMsgResponse.prototype.getCommandId = function () {
       return 0x020001;
}

module.exports.ProxyMsgResponse = ProxyMsgResponse;

//##############################
//####HeartBeat
//##############################

var HeartBeatRequest = function(){
}

HeartBeatRequest.prototype.readFrom = function (byteBuf){
}

HeartBeatRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x020002);
}

HeartBeatRequest.prototype.getCommandId = function () {
       return 0x020002;
}

module.exports.HeartBeatRequest = HeartBeatRequest;

var HeartBeatResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

}

HeartBeatResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
}

HeartBeatResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x020002);
       byteBuf.writeInt(this.errorNo);
}

HeartBeatResponse.prototype.getCommandId = function () {
       return 0x020002;
}

module.exports.HeartBeatResponse = HeartBeatResponse;
