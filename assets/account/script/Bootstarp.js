var AuthenticateHandler = require("AuthenticateHandler");
var ProxyNet = require("ProxyNet");

cc.Class({
    extends: cc.Component,

    // use this for initialization
    onLoad: function () {
        //开启日志
        cc._initDebugSetting(cc.DebugMode.INFO); 

        //登录模式处理
        if (DEBUG) {
            var node = this.node.getChildByName("btn-wechar");
            if (node != null) {
                this.node.removeChild(node);
            }
        } else {
            var node = this.node.getChildByName("btn-traveler");
            if (node != null) {
                this.node.removeChild(node);
            }
        }

        //协议处理方式定义
        var authenticateHandler = new AuthenticateHandler(this);
        authenticateHandler.handlerAuthenticateResponse = this.handlerAuthenticateResponse;
    },
    handlerAuthenticateResponse:function(rsp){
        
        dump(rsp); 
        
        for(var field in rsp){
            if(field === "errorNo"){
                continue;
            }
            ACCOUNT[field] = rsp[field];
        }
        
        ProxyNet.startHeartbeat();
        cc.director.loadScene('scene-lobby');
    }
});
