
//##############################
//####AccountProfile
//##############################
var AccountProfile = function(){
   /**
   *   展示名称
   **/
   this.name="";

   /**
   *   偏好对应的服务名称
   **/
   this.serviceName="";

   /**
   *   是否开启过
   **/
   this.open=false;

}

AccountProfile.prototype.readFrom= function ( byteBuf ) {
       this.name = byteBuf.readUTF8();
       this.serviceName = byteBuf.readUTF8();
       this.open = byteBuf.readBool();
}

AccountProfile.prototype.writeTo = function ( byteBuf ){
       byteBuf.writeUTF8(this.name);
       byteBuf.writeUTF8(this.serviceName);
       byteBuf.writeBool(this.open);
}

module.exports.AccountProfile = AccountProfile;

//##############################
//####ProfileInfo
//##############################
var ProfileInfo = function(){
   /**
   *   偏好对应的服务名称
   **/
   this.serviceName="";

   /**
   *   偏好名称
   **/
   this.name="";

   /**
   *   偏好图标
   **/
   this.icon="";

   /**
   *   偏好简介
   **/
   this.desc="";

   /**
   *   热度
   **/
   this.hot=0;

}

ProfileInfo.prototype.readFrom= function ( byteBuf ) {
       this.serviceName = byteBuf.readUTF8();
       this.name = byteBuf.readUTF8();
       this.icon = byteBuf.readUTF8();
       this.desc = byteBuf.readUTF8();
       this.hot = byteBuf.readInt();
}

ProfileInfo.prototype.writeTo = function ( byteBuf ){
       byteBuf.writeUTF8(this.serviceName);
       byteBuf.writeUTF8(this.name);
       byteBuf.writeUTF8(this.icon);
       byteBuf.writeUTF8(this.desc);
       byteBuf.writeInt(this.hot);
}

module.exports.ProfileInfo = ProfileInfo;

//##############################
//####Authenticate
//##############################

var AuthenticateRequest = function(){
   /**
   *   验证类别 0游客 1微信
   **/
   this.type = 0;

   /**
   *   第三方签名
   **/
   this.tocken = "";

   /**
   *   昵称
   **/
   this.name = "";

}

AuthenticateRequest.prototype.readFrom = function (byteBuf){
       this.type = byteBuf.readInt();
       this.tocken = byteBuf.readUTF8();
       this.name = byteBuf.readUTF8();
}

AuthenticateRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x100001);
       byteBuf.writeInt(this.type);
       byteBuf.writeUTF8(this.tocken);
       byteBuf.writeUTF8(this.name);
}

AuthenticateRequest.prototype.getCommandId = function () {
       return 0x100001;
}

module.exports.AuthenticateRequest = AuthenticateRequest;

var AuthenticateResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

   /**
   *   账户ID
   **/
   this.id = 0;

   /**
   *   货币1数量
   **/
   this.currency1 = 0;

   /**
   *   赞数量
   **/
   this.favour = 0;

   /**
   *   非赞数量
   **/
   this.unfavour = 0;

   /**
   *   是否是新账户
   **/
   this.isNew = false;

   /**
   *   配置列表
   **/
   this.profiles = new Array()

}

AuthenticateResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
       this.id = byteBuf.readLong();
       this.currency1 = byteBuf.readInt();
       this.favour = byteBuf.readInt();
       this.unfavour = byteBuf.readInt();
       this.isNew = byteBuf.readBool();

       var profilesLength = byteBuf.readInt();
       for(var i=0;i<profilesLength;i++){
           var temp = new AccountProfile;
           temp.readFrom(byteBuf);
           this.profiles.push(temp);
       }
}

AuthenticateResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x100001);
       byteBuf.writeInt(this.errorNo);
       byteBuf.writeLong(this.id);
       byteBuf.writeInt(this.currency1);
       byteBuf.writeInt(this.favour);
       byteBuf.writeInt(this.unfavour);
       byteBuf.writeBool(this.isNew);

       byteBuf.writeInt(this.profiles.length);
       for(var i=0;i<profiles.length;i++){
           this.profiles[i].writeTo(byteBuf);
       }
}

AuthenticateResponse.prototype.getCommandId = function () {
       return 0x100001;
}

module.exports.AuthenticateResponse = AuthenticateResponse;

//##############################
//####AddProfile
//##############################

var AddProfileRequest = function(){
   /**
   *   配置对应的服务名称
   **/
   this.serviceNames = new Array();

}

AddProfileRequest.prototype.readFrom = function (byteBuf){

       var serviceNamesLength = byteBuf.readInt();
       for(var i=0;i<serviceNamesLength;i++){
           this.serviceNames.push(byteBuf.readUTF8());
       }
}

AddProfileRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110001);

       byteBuf.writeInt(this.serviceNames.length);
       for(var i=0;i<serviceNames.size();i++){
           byteBuf.writeUTF8(this.serviceNames[i]);
       }
}

AddProfileRequest.prototype.getCommandId = function () {
       return 0x110001;
}

module.exports.AddProfileRequest = AddProfileRequest;

var AddProfileResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

   /**
   *   新增的配置列表
   **/
   this.serviceNames = new Array()

}

AddProfileResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();

       var serviceNamesLength = byteBuf.readInt();
       for(var i=0;i<serviceNamesLength;i++){
           this.serviceNames.push(byteBuf.readUTF8());
       }
}

AddProfileResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110001);
       byteBuf.writeInt(this.errorNo);

       byteBuf.writeInt(this.serviceNames.length);
       for(var i=0;i<serviceNames.length;i++){
           byteBuf.writeUTF8(this.serviceNames[i]);
       }
}

AddProfileResponse.prototype.getCommandId = function () {
       return 0x110001;
}

module.exports.AddProfileResponse = AddProfileResponse;

//##############################
//####RemoveProfile
//##############################

var RemoveProfileRequest = function(){
   /**
   *   配置对应的
   **/
   this.serviceName = "";

}

RemoveProfileRequest.prototype.readFrom = function (byteBuf){
       this.serviceName = byteBuf.readUTF8();
}

RemoveProfileRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110002);
       byteBuf.writeUTF8(this.serviceName);
}

RemoveProfileRequest.prototype.getCommandId = function () {
       return 0x110002;
}

module.exports.RemoveProfileRequest = RemoveProfileRequest;

var RemoveProfileResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

}

RemoveProfileResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
}

RemoveProfileResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110002);
       byteBuf.writeInt(this.errorNo);
}

RemoveProfileResponse.prototype.getCommandId = function () {
       return 0x110002;
}

module.exports.RemoveProfileResponse = RemoveProfileResponse;

//##############################
//####OpenProfile
//##############################

var OpenProfileRequest = function(){
   /**
   *   点击的Profile服务名称
   **/
   this.serviceName = "";

}

OpenProfileRequest.prototype.readFrom = function (byteBuf){
       this.serviceName = byteBuf.readUTF8();
}

OpenProfileRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110003);
       byteBuf.writeUTF8(this.serviceName);
}

OpenProfileRequest.prototype.getCommandId = function () {
       return 0x110003;
}

module.exports.OpenProfileRequest = OpenProfileRequest;

var OpenProfileResponse = function(){
   /**
   *   响应码
   **/
   this.errorNo = 0;

}

OpenProfileResponse.prototype.readFrom = function (byteBuf){
       this.errorNo = byteBuf.readInt();
}

OpenProfileResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0x110003);
       byteBuf.writeInt(this.errorNo);
}

OpenProfileResponse.prototype.getCommandId = function () {
       return 0x110003;
}

module.exports.OpenProfileResponse = OpenProfileResponse;

//##############################
//####ActivityProfile
//##############################

var ActivityProfileRequest = function(){
   /**
   *   当前地理位置X轴
   **/
   this.posx = 0;

   /**
   *   当前地理位置Y轴
   **/
   this.posy = 0;

}

ActivityProfileRequest.prototype.readFrom = function (byteBuf){
       this.posx = byteBuf.readDouble();
       this.posy = byteBuf.readDouble();
}

ActivityProfileRequest.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0X040001);
       byteBuf.writeDouble(this.posx);
       byteBuf.writeDouble(this.posy);
}

ActivityProfileRequest.prototype.getCommandId = function () {
       return 0X040001;
}

module.exports.ActivityProfileRequest = ActivityProfileRequest;

var ActivityProfileResponse = function(){
   /**
   *   区域推荐配置列表
   **/
   this.lbsProfiles = new Array()

   /**
   *   热度推荐配置列表
   **/
   this.hotProfiles = new Array()

}

ActivityProfileResponse.prototype.readFrom = function (byteBuf){

       var lbsProfilesLength = byteBuf.readInt();
       for(var i=0;i<lbsProfilesLength;i++){
           var temp = new ProfileInfo;
           temp.readFrom(byteBuf);
           this.lbsProfiles.push(temp);
       }

       var hotProfilesLength = byteBuf.readInt();
       for(var i=0;i<hotProfilesLength;i++){
           var temp = new ProfileInfo;
           temp.readFrom(byteBuf);
           this.hotProfiles.push(temp);
       }
}

ActivityProfileResponse.prototype.writeTo = function (byteBuf){
       byteBuf.writeInt(0X040001);

       byteBuf.writeInt(this.lbsProfiles.length);
       for(var i=0;i<lbsProfiles.length;i++){
           this.lbsProfiles[i].writeTo(byteBuf);
       }

       byteBuf.writeInt(this.hotProfiles.length);
       for(var i=0;i<hotProfiles.length;i++){
           this.hotProfiles[i].writeTo(byteBuf);
       }
}

ActivityProfileResponse.prototype.getCommandId = function () {
       return 0X040001;
}

module.exports.ActivityProfileResponse = ActivityProfileResponse;
