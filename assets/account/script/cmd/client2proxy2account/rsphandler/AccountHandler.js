var cmds = require("Client2proxy2accountCmds");

var AccountHandler = function(scence){


    this.scence = scence;
    this.servicename="client2proxy2account";

    this.exec = function(commandId,bytebuf){
      switch(commandId){
           case 0x110001 :{
               var rsp = new cmds.AddProfileResponse();
               rsp.readFrom(bytebuf);
               this.handlerAddProfileResponse(rsp);
               break;
           }
           case 0x110002 :{
               var rsp = new cmds.RemoveProfileResponse();
               rsp.readFrom(bytebuf);
               this.handlerRemoveProfileResponse(rsp);
               break;
           }
           case 0x110003 :{
               var rsp = new cmds.OpenProfileResponse();
               rsp.readFrom(bytebuf);
               this.handlerOpenProfileResponse(rsp);
               break;
           }
        }
      }

    //模块处理映射注册
    MODULE_MAPPING[0X11] = this;

}

module.exports = AccountHandler;
