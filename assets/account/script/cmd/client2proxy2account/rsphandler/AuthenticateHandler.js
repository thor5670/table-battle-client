var cmds = require("Client2proxy2accountCmds");

var AuthenticateHandler = function(scence){


    this.scence = scence;
    this.servicename="client2proxy2account";

    this.exec = function(commandId,bytebuf){
      switch(commandId){
           case 0x100001 :{
               var rsp = new cmds.AuthenticateResponse();
               rsp.readFrom(bytebuf);
               this.handlerAuthenticateResponse(rsp);
               break;
           }
        }
      }

    //模块处理映射注册
    MODULE_MAPPING[0X10] = this;

}

module.exports = AuthenticateHandler;
