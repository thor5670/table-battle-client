var cmds = require("Client2proxy2accountCmds");

var ProfileHandler = function(scence){


    this.scence = scence;
    this.servicename="client2proxy2account";

    this.exec = function(commandId,bytebuf){
      switch(commandId){
           case 0X040001 :{
               var rsp = new cmds.ActivityProfileResponse();
               rsp.readFrom(bytebuf);
               this.handlerActivityProfileResponse(rsp);
               break;
           }
        }
      }

    //模块处理映射注册
    MODULE_MAPPING[0X12] = this;

}

module.exports = ProfileHandler;
