var cmds = require("Client2proxy2accountCmds");
var proxyNet = require("ProxyNet");

cc.Class({
    extends: cc.Component,
    _sendAuthen: function () {
        var selfNode = this.node;
        cc.loader.loadRes("common/prefab/loading", function (err, loading) {
            if(err){
                cc.log(err);
            }else{
                selfNode.parent.addChild(cc.instantiate(loading));
            }
        });

        var localTravelerTocken = cc.sys.localStorage.getItem("travelerTocken");
        if (localTravelerTocken == null) {
            localTravelerTocken = md5(new Date().getTime());
            cc.sys.localStorage.setItem("travelerTocken", localTravelerTocken);
        }

        cc.log(localTravelerTocken);

        var authenticateReq = new cmds.AuthenticateRequest();
        authenticateReq.type = 0;
        authenticateReq.tocken = localTravelerTocken;
        authenticateReq.name = "GUEST " + parseInt(Math.random()*1000);

        ACCOUNT.name = authenticateReq.name;
        
        //开启链接
        proxyNet.conn(function () {
            //链接准备好以后,发送验证
            proxyNet.send("client2proxy2account", "", authenticateReq);
        });
    },
    onEnable: function () {
        this.node.on(cc.Node.EventType.TOUCH_END, this._sendAuthen, this);
    },
    onDisable: function () {
        this.node.off(cc.Node.EventType.TOUCH_END, this._sendAuthen, this);
    }
});
